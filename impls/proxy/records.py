"""proxy records"""
import abc
from dlkit.abstract_osid.osid import records as osid_records


class ProxyRecord(osid_records.OsidRecord):
    """A record for a ``Proxy``.

    The methods specified by the record type are available through the
    underlying object.

    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        super(ProxyRecord, self).__init__()

    def implements_record_type(self, record_type):
        return False


class ProxyConditionRecord(osid_records.OsidRecord):
    """A record for a ``ProxyCondition``.

    The methods specified by the record type are available through the
    underlying object.

    """
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        super(ProxyConditionRecord, self).__init__()

    def implements_record_type(self, record_type):
        return False
