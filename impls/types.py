"""some initial types"""
# -*- coding: utf-8 -*-
from dlkit_django.impls.osid.osid_errors import NotFound
from dlkit_django.impls.primordium import Id


class Genus(object):
    """genus type"""

    generic_types = {
        'DEFAULT': 'Default',
        'UNKNOWN': 'Unknown'
    }

    def __init__(self):
        self.type_set = {
            'Gen': self.generic_types
        }

    def get_type_data(self, name):
        """get type data"""
        try:
            return {
                'authority': 'dlkit.mit.edu',
                'namespace': 'GenusType',
                'identifier': name,
                'domain': 'Generic Types',
                'display_name': self.generic_types[name] + ' Generic Type',
                'display_label': self.generic_types[name],
                'description': ('The ' + self.generic_types[name] +
                                ' Type. This type has no symantic meaning.')
            }
        except IndexError:
            raise NotFound('GenusType: ' + name)


class Language(object):
    """language type"""
    def get_type_data(self, name):
        """get type data"""
        if name == 'DEFAULT':
            from .profile import LANGUAGETYPEID
            return Id(LANGUAGETYPEID)
        else:
            raise NotFound('DEFAULT Language Type')


class Script(object):
    """script type"""
    def get_type_data(self, name):
        """get type data"""
        if name == 'DEFAULT':
            from .profile import SCRIPTTYPEID
            return Id(SCRIPTTYPEID)
        else:
            raise NotFound('DEFAULT Script Type')


class Format(object):
    """format type"""

    def get_type_data(self, name):
        """get type data"""
        if name == 'DEFAULT':
            from .profile import FORMATTYPEID
            return Id(FORMATTYPEID)
        else:
            raise NotFound('DEFAULT Format Type')
