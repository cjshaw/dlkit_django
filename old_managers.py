"""
This module contains the OSID RuntimeManager class implementation for Django
"""
import importlib

from dlkit_django.impls.osid.managers import OsidRuntimeManager
from dlkit_django.impls.configuration.objects import Configuration
from dlkit_django.impls.configuration.sessions import ValueRetrievalSession
from dlkit_django.impls.primitives import Id
from dlkit_django import configs
from dlkit_django.registry import MANAGER_PATHS

NO_PROXY = 0
PROXY = 1


class Runtime(OsidRuntimeManager):
    """runtime for local configs of dlkit"""

    def __init__(self, config=None):
        if config is None:
            self._configuration = Configuration((getattr(configs, 'BOOTSTRAP')))
        else:
            self._configuration = config
        self._managers = {}
        self._proxy_managers = {}
        super(Runtime, self).__init__()

    def get_service_manager(self, osid, proxy=None, version=(3, 0, 0)):
        """get dlkit services manager of given name"""
        if proxy is None:
            proxy_key = NO_PROXY
        else:
            proxy_key = PROXY
        return self._load_mgr(osid, 'SERVICE', version, proxy_key, proxy)

    def get_manager(self, osid=None, implementation=None, version=(3, 0, 0)):
        """get dlkit manager from specific impl"""
        if implementation + osid not in self._managers:
            self._managers[implementation + osid] = self._load_mgr(osid,
                                                                   implementation,
                                                                   version,
                                                                   0)
        return self._managers[implementation + osid]

    def get_proxy_manager(self, osid=None, implementation=None, version=(3, 0, 0)):
        """get proxy manager from specific impl"""
        if implementation + osid not in self._proxy_managers:
            self._proxy_managers[implementation + osid] = self._load_mgr(osid,
                                                                         implementation,
                                                                         version,
                                                                         1)
        return self._proxy_managers[implementation + osid]

    def get_configuration(self):
        return ValueRetrievalSession(self._configuration)

    def _load_mgr(self, osid, implementation, version, proxy_key, proxy=None):
        """load the actual manager"""
        new_configuration = Configuration(getattr(configs, implementation))
        parameter_id = Id('parameter:implKey@dlkit_runtime')
        impl_key = ValueRetrievalSession(
            new_configuration).get_value_by_parameter(
                parameter_id).get_string_value()
        module_path = '.'.join(MANAGER_PATHS[impl_key][osid][proxy_key].split('.')[:-1])
        class_name = MANAGER_PATHS[impl_key][osid][proxy_key].split('.')[-1]
        module = importlib.import_module(module_path)
        if proxy is None:
            manager = getattr(module, class_name)()
        else:
            manager = getattr(module, class_name)(proxy=proxy)
        manager.initialize(Runtime(new_configuration))
        return manager

    configuration = property(fget=get_configuration)

    def get_license(self):
        """stub"""
        return ''

    license_ = property(fget=get_license)